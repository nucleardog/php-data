# Changelog

## Unreleased

[View Commits](https://gitlab.com/nucleardog/php-data/-/compare/v0.0.6...master)

## v0.0.6 (2024-08-09)

[View Commits](https://gitlab.com/nucleardog/php-data/-/commits/v0.0.5...v0.0.6)

* Various bug fixes and updates from using in real-world projects.

## v0.0.5 (2024-04-26)

[View Commits](https://gitlab.com/nucleardog/php-data/-/commits/v0.0.4...v0.0.5)

* When a new instance of the Data class is returned (e.g., for nested array/object
  values), it will now use the containing class name to allow extending the Data
  class.

## v0.0.4 (2024-04-26)

[View Commits](https://gitlab.com/nucleardog/php-data/-/commits/v0.0.3...v0.0.4)

* Add toCollection and toArray methods for converting Data objects to a Laravel
  Collection and plain array, respectively.
* Add pass-through to Collection class to allow using Collection methods directly
  on the data object. (See [examples/collections.php](examples/collections.php).)
* Fix: Don't rewrap Data objects in an Accessor.

## v0.0.3 (2024-04-26)

[View Commits](https://gitlab.com/nucleardog/php-data/-/commits/v0.0.2...v0.0.3)

* Simplified Accessors by moving responsibility for wrapping returned values
  entirely to the Data class.
* Added support for iterating on the Data class.
* Improved tests.

## v0.0.2 (2024-04-23)

[View Commits](https://gitlab.com/nucleardog/php-data/-/commits/v0.0.1...v0.0.2)

* Added `get`, `has`, `set` and `forget` methods supporting "dot notation" as
  well as paths given in an array for working with nested values.

## v0.0.1 (2024-04-23)

[View Commits](https://gitlab.com/nucleardog/php-data/-/commits/v0.0.1)

* Initial WIP commit.