<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;

class ApiResponseDataObjectTest extends ApiResponseDataTestCase
{
	use Concerns\TestsWithObjectProperties;
}