<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;
use PHPUnit\Framework\Attributes\DataProvider;
use Nucleardog\Data\Data;

class DotNotationTest extends DataTestCase
{

	#[DataProvider('apiResponseData')]
	public function testHas(Data $data): void
	{
		// Simple case
		$this->assertSame(true, $data->has('status'));
		// Nested value
		$this->assertSame(true, $data->has('data.User.id'));
		// Alternate syntax
		$this->assertSame(true, $data->has(['data', 'User', 'id']));
		// Non-existent final key + alternate syntax
		$this->assertSame(false, $data->has('data.User.foo'));
		$this->assertSame(false, $data->has(['data', 'User', 'foo']));
		// Non-existent key in path + alternate syntax
		$this->assertSame(false, $data->has('data.NonExistent.foo'));
		$this->assertSame(false, $data->has(['data', 'NonExistent', 'foo']));
	}

	#[DataProvider('apiResponseData')]
	public function testGet(Data $data): void
	{
		// Simple case
		$this->assertSame('success', $data->get('status'));
		// Nested value
		$this->assertSame(1234, $data->get('data.User.id'));
		// Alternate syntax
		$this->assertSame(1234, $data->get(['data', 'User', 'id']));
	}

	#[DataProvider('apiResponseData')]
	public function testGetNonexistentFinalKey(Data $data): void
	{
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key foo');
		$data->get('data.User.foo');
	}

	#[DataProvider('apiResponseData')]
	public function testGetNonexistentKeyInPath(Data $data): void
	{
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key NonExistent');
		$data->get('data.NonExistent.foo');
	}

	#[DataProvider('apiResponseData')]
	public function testGetNonTraversableKeyInPath(Data $data): void
	{
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Path "data.User.id.foo" traverses unsupported data type.');
		$data->get('data.User.id.foo');
	}

	#[DataProvider('apiResponseData')]
	public function testSetExistingKey(Data $data): void
	{
		$data->set('data.User.id', 5678);
		$this->assertSame(5678, $data->get('data.User.id'));
	}

	#[DataProvider('apiResponseData')]
	public function testSetNewKey(Data $data): void
	{
		$data->set('data.User.active', false);
		$this->assertSame(true, $data->has('data.User.active'));
		$this->assertSame(false, $data->get('data.User.active'));
	}

	#[DataProvider('apiResponseData')]
	public function testSetNonexistentKeyInPath(Data $data): void
	{
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key NonExistent');
		$data->set('data.NonExistent.foo', 1234);
	}

	#[DataProvider('apiResponseData')]
	public function testSetNonTraversableKeyInPath(Data $data): void
	{
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Path "data.User.id.foo" traverses unsupported data type.');
		$data->get('data.User.id.foo', 1234);
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetExistingKey(Data $data): void
	{
		$data->forget('data.User.id');
		$this->assertSame(false, $data->has('data.User.id'));
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key id');
		$data->get('data.User.id');
	}

	#[DataProvider('apiResponseData')]
	public function testSetThenUnsetKey(Data $data): void
	{
		$this->assertSame(false, $data->has('data.User.active'));
		$data->set('data.User.active', true);
		$this->assertSame(true, $data->has('data.User.active'));
		$this->assertSame(true, $data->get('data.User.active'));
		$data->forget('data.User.active');
		$this->assertSame(false, $data->has('data.User.active'));
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key active');
		$data->get('data.User.active');
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetNonexistentFinalKey(Data $data): void
	{
		$data->forget('data.User.country');
		$this->expectNotToPerformAssertions();
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetNonexistentKeyInPath(Data $data): void
	{
		$data->forget('data.NonExistent.id');
		$this->expectNotToPerformAssertions();
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetNonTraversableKeyInPath(Data $data): void
	{
		$data->forget('data.User.id.foo');
		$this->expectNotToPerformAssertions();
	}

	#[DataProvider('apiResponseData')]
	public function testHasArray(Data $data): void
	{
		$this->assertSame(true, $data->has(['data', 'User', 'id']));
	}

	#[DataProvider('apiResponseData')]
	public function testGetArray(Data $data): void
	{
		$this->assertSame(1234, $data->get(['data', 'User', 'id']));
	}

	#[DataProvider('apiResponseData')]
	public function testSetArray(Data $data): void
	{
		$this->assertNotSame(5678, $data->get(['data', 'User', 'id']));
		$data->set(['data', 'User', 'id'], 5678);
		$this->assertSame(5678, $data->get(['data', 'User', 'id']));
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetArray(Data $data): void
	{
		$this->assertSame(true, $data->has(['data', 'User', 'id']));
		$data->forget(['data', 'User', 'id']);
		$this->assertSame(false, $data->has(['data', 'User', 'id']));
	}

}