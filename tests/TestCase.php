<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;

use PHPUnit\Framework\TestCase as PhpUnitTestCase;

abstract class TestCase extends PhpUnitTestCase
{

}