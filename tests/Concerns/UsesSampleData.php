<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests\Concerns;
use Nucleardog\Data\Data;

trait UsesSampleData
{
	protected Data $data;

	public static function simpleData(): array
	{
		return static::createSampleDataCases([
			'a' => 1,
			'b' => 2,
			'c' => 'cat',
		]);
	}

	public static function apiResponseData(): array
	{
		return static::createSampleDataCases([
			'status' => 'success',
			'data' => [
				'User' => [
					'id' => 1234,
					'email' => 'test@user.com',
					'name' => 'Test User',
					'company' => [
						'id' => 5678,
						'name' => 'My Company',
					],
				],
			],
		]);
	}

	private static function createSampleDataCases(array $data): array
	{
		return [
			// Using a simple array
			[new Data($data)],
			// Using an object containing arrays
			[new Data((object)$data)],
			// Using an object containing objects
			[new Data(json_decode(json_encode($data), false))],
			/*
 			 * ArrayAccess tests disabled as they fail most set + get operations.
			// Using a class exposing data through ArrayAccess
			[new Data(static::createArrayAccessWrapper($data))],
			// Array containing nested ArrayAccess objects
			[new Data(array_map(fn($value) => is_array($value) ? static::createArrayAccessWrapper($value) : $value, $data))],
			// Object with ArrayAccess objects on properties
			[new Data((object)array_map(fn($value) => is_array($value) ? static::createArrayAccessWrapper($value) : $value, $data))],
			 */
		];
	}

	private static function createArrayAccessWrapper(array $data): \ArrayAccess
	{
		return new class($data) implements \ArrayAccess, \Countable, \IteratorAggregate {
			public function __construct(private array $data) { }
			public function offsetExists(mixed $offset): bool { return isset($this->data[$offset]); }
			public function offsetGet(mixed $offset): mixed { return $this->data[$offset]; }
			public function offsetSet(mixed $offset, mixed $value): void { $this->data[$offset] = $value; }
			public function offsetUnset(mixed $offset): void { unset($this->data[$offset]); }
			public function count(): int { return count($this->data); }
			public function getIterator(): \Traversable { return new \ArrayIterator($this->data); }
		};
	}

}