<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests\Concerns;
use Nucleardog\Data\Data;

trait TestsWithObjectProperties
{

	protected function issetKey(Data $data, string $key): bool
	{
		return isset($data->$key);
	}

	protected function getKey(Data $data, string $key): mixed
	{
		return $data->$key;
	}

	protected function setKey(Data $data, string $key, mixed $value): void
	{
		$data->$key = $value;
	}

	protected function unsetKey(Data $data, string $key): void
	{
		unset($data->$key);
	}

}
