<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;

class SimpleDataArrayTest extends SimpleDataTestCase
{
	use Concerns\TestsWithArrayAccess;
}