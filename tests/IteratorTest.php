<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;
use PHPUnit\Framework\Attributes\DataProvider;
use Nucleardog\Data\Data;

class IteratorTest extends DataTestCase
{

	#[DataProvider('apiResponseData')]
	public function testDataWrapsIterator(Data $data): void
	{
		$this->assertInstanceOf(\Nucleardog\Data\Iterators\DataWrapIterator::class, $data->getIterator());
	}

	#[DataProvider('apiResponseData')]
	public function testIteratorWrapsData(Data $data): void
	{
		foreach ($data as $k=>$v) {
			switch ($k) {
				case 'status':
					$this->assertSame('success', $v);
					break;
				case 'data':
					$this->assertInstanceOf(Data::class, $v);
					break;

			}
		}
	}

	#[DataProvider('apiResponseData')]
	public function testIteratorKeepsReferences(Data $data): void
	{
		$this->assertNotSame(5678, $data->get('data.User.id'));
		foreach ($data['data'] as $k=>$v) {
			if ($k === 'User') {
				$v['id'] = 5678;
			}
		}
		$this->assertSame(5678, $data->get('data.User.id'));
	}


}