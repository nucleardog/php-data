<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;

class ApiResponseDataArrayTest extends ApiResponseDataTestCase
{
	use Concerns\TestsWithArrayAccess;
}