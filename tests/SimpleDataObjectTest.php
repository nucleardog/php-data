<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;

class SimpleDataObjectTest extends SimpleDataTestCase
{
	use Concerns\TestsWithObjectProperties;
}
