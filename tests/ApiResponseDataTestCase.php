<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;
use PHPUnit\Framework\Attributes\DataProvider;
use Nucleardog\Data\Data;

abstract class ApiResponseDataTestCase extends DataTestCase
{

	protected abstract function issetKey(Data $data, string $key): bool;
	protected abstract function getKey(Data $data, string $key): mixed;
	protected abstract function setKey(Data $data, string $key, mixed $value): void;
	protected abstract function unsetKey(Data $data, string $key): void;

	#[DataProvider('apiResponseData')]
	public function testCheckKeysExist(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'status'));
		$this->assertSame(true, $this->issetKey($data, 'data'));
		$this->assertSame(false, $this->issetKey($data, 'foo'));
	}

	#[DataProvider('apiResponseData')]
	public function testGetKeys(Data $data): void
	{
		$this->assertSame('success', $this->getKey($data, 'status'));
		// No assertion, but make sure this doesn't throw an exception.
		$this->getKey($data, 'data');
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key foo');
		$this->getKey($data, 'foo');
	}

	#[DataProvider('apiResponseData')]
	public function testSetKeys(Data $data): void
	{
		$this->assertNotSame('failure', $this->getKey($data, 'status'));
		$this->assertSame(false, $this->issetKey($data, 'foo'));
		// Set values
		$this->setKey($data, 'status', 'failure');
		$this->setKey($data, 'foo', 'bar');
		// Check values stuck
		$this->assertSame('failure', $this->getKey($data, 'status'));
		$this->assertSame('bar', $this->getKey($data, 'foo'));
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetKeys(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'status'));
		$this->assertSame(true, $this->issetKey($data, 'data'));
		$this->unsetKey($data, 'status');
		$this->unsetKey($data, 'data');
		$this->assertSame(false, $this->issetKey($data, 'status'));
		$this->assertSame(false, $this->issetKey($data, 'data'));
	}

	#[DataProvider('apiResponseData')]
	public function testSetThenIssetThenGet(Data $data): void
	{
		$this->assertSame(false, $this->issetKey($data, 'foo'));
		$this->setKey($data, 'foo', 'bar');
		$this->assertSame(true, $this->issetKey($data, 'foo'));
		$this->assertSame('bar', $this->getKey($data, 'foo'));
	}

	#[DataProvider('apiResponseData')]
	public function testUnsetThenIssetThenGet(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'status'));
		$this->unsetKey($data, 'status');
		$this->assertSame(false, $this->issetKey($data, 'status'));
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key status');
		$this->getKey($data, 'status');
	}

	#[DataProvider('apiResponseData')]
	public function testAccessNestedData(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'data'));

		$nested = $this->getKey($data, 'data');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$nested = $this->getKey($nested, 'User');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$this->assertSame(true, $this->issetKey($nested, 'id'));
		$this->assertSame(true, $this->issetKey($nested, 'name'));
		$this->assertSame(false, $this->issetKey($nested, 'foo'));

		$this->assertSame(1234, $this->getKey($nested, 'id'));
		$this->assertSame('Test User', $this->getKey($nested, 'name'));
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key foo');
		$this->assertSame(1234, $this->getKey($nested, 'foo'));
	}

	#[DataProvider('apiResponseData')]
	public function testSetNestedData(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'data'));

		$nested = $this->getKey($data, 'data');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$nested = $this->getKey($nested, 'User');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$this->assertNotSame(5678, $this->getKey($nested, 'id'));

		$this->setKey($nested, 'id', 5678);

		$this->assertSame(5678, $this->getKey($nested, 'id'));
	}

	#[DataProvider('apiResponseData')]
	public function testSetNestedDataRetained(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'data'));

		// Set the value
		$nested = $this->getKey($data, 'data');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$nested = $this->getKey($nested, 'User');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$this->assertNotSame(5678, $this->getKey($nested, 'id'));

		$this->setKey($nested, 'id', 5678);

		$this->assertSame(5678, $this->getKey($nested, 'id'));

		// Then work back from the root to see if we see the same value
		$nested = $this->getKey($data, 'data');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$nested = $this->getKey($nested, 'User');
		$this->assertInstanceOf(\Nucleardog\Data\Data::class, $nested);

		$this->assertSame(5678, $this->getKey($nested, 'id'));
	}

}