<?php

declare(strict_types=1);

namespace Nucleardog\Data\Tests;
use PHPUnit\Framework\Attributes\DataProvider;
use Nucleardog\Data\Data;

abstract class SimpleDataTestCase extends DataTestCase
{

	protected abstract function issetKey(Data $data, string $key): bool;
	protected abstract function getKey(Data $data, string $key): mixed;
	protected abstract function setKey(Data $data, string $key, mixed $value): void;
	protected abstract function unsetKey(Data $data, string $key): void;

	#[DataProvider('simpleData')]
	public function testCheckKeysExist(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'a'));
		$this->assertSame(true, $this->issetKey($data, 'b'));
		$this->assertSame(true, $this->issetKey($data, 'c'));
		$this->assertSame(false, $this->issetKey($data, 'd'));
	}

	#[DataProvider('simpleData')]
	public function testGetKeys(Data $data): void
	{
		$this->assertSame(1, $this->getKey($data, 'a'));
		$this->assertSame(2, $this->getKey($data, 'b'));
		$this->assertSame('cat', $this->getKey($data, 'c'));
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key d');
		$this->getKey($data, 'd');
	}

	#[DataProvider('simpleData')]
	public function testSetKeys(Data $data): void
	{
		$this->assertNotSame(4, $this->getKey($data, 'a'));
		$this->assertNotSame(5, $this->getKey($data, 'b'));
		$this->assertNotSame('coffee', $this->getKey($data, 'c'));
		$this->assertSame(false, $this->issetKey($data, 'd'));
		// Set values
		$this->setKey($data, 'a', 4);
		$this->setKey($data, 'b', 5);
		$this->setKey($data, 'c', 'coffee');
		$this->setKey($data, 'd', 'donut');
		// Check values stuck
		$this->assertSame(4, $this->getKey($data, 'a'));
		$this->assertSame(5, $this->getKey($data, 'b'));
		$this->assertSame('coffee', $this->getKey($data, 'c'));
		$this->assertSame('donut', $this->getKey($data, 'd'));
	}

	#[DataProvider('simpleData')]
	public function testUnsetKeys(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'a'));
		$this->assertSame(true, $this->issetKey($data, 'b'));
		$this->assertSame(true, $this->issetKey($data, 'c'));
		$this->unsetKey($data, 'a');
		$this->unsetKey($data, 'b');
		$this->unsetKey($data, 'c');
		$this->assertSame(false, $this->issetKey($data, 'a'));
		$this->assertSame(false, $this->issetKey($data, 'b'));
		$this->assertSame(false, $this->issetKey($data, 'c'));
	}

	#[DataProvider('simpleData')]
	public function testSetThenIssetThenGet(Data $data): void
	{
		$this->assertSame(false, $this->issetKey($data, 'd'));
		$this->setKey($data, 'd', 'dog');
		$this->assertSame(true, $this->issetKey($data, 'd'));
		$this->assertSame('dog', $this->getKey($data, 'd'));
	}

	#[DataProvider('simpleData')]
	public function testUnsetThenIssetThenGet(Data $data): void
	{
		$this->assertSame(true, $this->issetKey($data, 'c'));
		$this->unsetKey($data, 'c');
		$this->assertSame(false, $this->issetKey($data, 'c'));
		$this->expectException(\OutOfBoundsException::class);
		$this->expectExceptionMessage('Data does not contain key c');
		$this->getKey($data, 'c');
	}

}