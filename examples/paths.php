<?php

declare(strict_types=1);
require(__DIR__.'/../vendor/autoload.php');

use Nucleardog\Data\Data;

$response = new Data([
	'status' => 'success',
	'data' => [
		'User' => [
			'id' => 1234,
			'name' => 'Adam',
			'company.id' => 5678,
			'company.name' => 'Test Company',
		],
	],
]);

////////////////////////////////////////////////////////////////////////////////

// The data class provides has() and get() methods to allow more easily working
// with deeply nested data.

// You can use these methods with "dot notation":
if ($response->has('data.User.id')) {
	echo sprintf('User ID: %s', $response->get('data.User.id')).PHP_EOL;
}

// If you want to work with data that contains dots in the keys or something, you
// can also provide a pre-split path as an array.
if ($response->has(['data', 'User', 'company.id'])) {
	echo sprintf('Company ID: %s', $response->get(['data', 'User', 'company.id'])).PHP_EOL;
}



////////////////////////////////////////////////////////////////////////////////

// The data class also provides set() and forget() methods for writing data back.
// These also support both the "dot notation" and explicit arrays.

$response->set('data.User.id', 3333);
echo sprintf('New User ID: %s', $response->get('data.User.id')).PHP_EOL;

// You can also create new keys:
$response->set('data.User.country', 'Canada');
echo sprintf('New User Country: %s', $response->get('data.User.country')).PHP_EOL;

// And unset keys
$response->forget('data.User.country');
echo sprintf('Has country? %s', $response->has('data.User.country') ? 'yes' : 'no').PHP_EOL;



////////////////////////////////////////////////////////////////////////////////

// Some edge cases...

// You can use has() to check keys under non-existent or non-traversable properties:
$response->has('data.Invoice.id');
$response->has('data.User.id.foo');

// Attempting to fetch a non-existent key or traverse a non-traversable property
// will throw an OutOfBoundsException
//$response->get('data.Invoice.id');
//$response->get('data.User.id.foo');

// Setting a new key at a non-existent path will, likewise, throw an OutOfBoundsException
// (We don't know whether address should be created as an array, object, or something
//  else entirely, so you must explicitly set it first.)
//$response->set('data.User.address.country', 'Canada');

// Trying to unset a key that doesn't exist will not throw an exception if it
// encounters a situation where the key you specified does not / cannot be set.
$response->forget('data.Invoice.id'); // Yep, it's not set. Never was.
$response->forget('data.User.id.foo'); // Yep, it's not set. Can't be.