<?php

declare(strict_types=1);
require(__DIR__.'/../vendor/autoload.php');

use Nucleardog\Data\Data;

////////////////////////////////////////////////////////////////////////////////

// If the `illuminate/collections` package is installed, the Data object gains
// support for all the normal Laravel collection methods.

// NOTE: This package does not install illuminate/collections by default. You
//       will need to explicitly `composer require` it to run this example.


// Build some sort of data object.
$response = new Data([
	'status' => 'success',
	'data' => [
		'User' => [
			[ 'id' => 1234, 'name' => 'Adam' ],
			[ 'id' => 5678, 'name' => 'Lesley' ],
		],
	],
]);


// Fetch the name for each user
echo sprintf(
	'User #1234: %s',
	$response['data']['User']->where('id', 1234)->pluck('name')->first(),
).PHP_EOL;

echo sprintf(
	'User #5678: %s',
	$response['data']['User']->where('id', 5678)->pluck('name')->first(),
).PHP_EOL;


// Alternatively, we could use each() to act on each entry
$response['data']['User']->each(function(Data $user) {
	// All arrays/objects/etc will be wrapped in another Data object just
	// like when they're accessed directly. You can still mix-and-match
	// syntax.
	echo sprintf(
		'User #%d: %s',
		$user->id,
		$user['name'],
	).PHP_EOL;

});


// The data instance works like a PHP object in that it maintains a reference
// or connection to the original data, so modifications will be reflected back.
$response['data']['User']->where('id', 1234)->each(fn(Data $user) => $user->id = 9999);

echo sprintf(
	'User #%d: %s', // prints id 9999
	$response['data']['User'][0]['id'],
	$response['data']['User'][0]['name'],
).PHP_EOL;


// However methods that return a new collection are not modifying the underlying
// collections.
$filtered = $response['data']['User']->filter(fn(Data $user) => $user->id === 5678);
echo sprintf(
	'Original object contains %d items.'.PHP_EOL. // prints 2
	'Filtered object contains %d items.'.PHP_EOL, // prints 1
	$response['data']['User']->count(),
	$filtered->count(),
);