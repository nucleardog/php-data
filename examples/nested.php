<?php

declare(strict_types=1);
require(__DIR__.'/../vendor/autoload.php');

use Nucleardog\Data\Data;

////////////////////////////////////////////////////////////////////////////////

// The data class can be instantiated with an object, array, ArrayAccess (with limitations), etc.
//
// When accessing keys, if they can be wrapped in the Data class as well, they
// will be. You can access nested properties using the same mixed syntax and
// helpers as the initial object.

$response1 = new Data([
	'status' => 'success',
	'data' => [
		'User' => [
			'id' => 1234,
			'name' => 'Adam',
		],
	],
]);

$response2 = new Data((object)[
	'status' => 'success',
	// You can also mix and match types. This is not an object.
	'data' => [
		'User' => (object)[
			'id' => 1234,
			'name' => 'Adam',
		],
	],
]);

echo "Response 1 type: ".gettype($response1->unwrap()).PHP_EOL;
echo "Response 2 type: ".gettype($response2->unwrap()).PHP_EOL;
echo PHP_EOL;



////////////////////////////////////////////////////////////////////////////////

// You can then mix accessing keys via object properties or array syntax freely.

if (isset($response1->data['User']->id)) {
	echo "Response 1 User Id: ".$response1->data['User']->id.PHP_EOL;
}
if (isset($response2['data']->User['id'])) {
	echo "Response 1 User Id: ".$response2['data']->User['id'].PHP_EOL;
}

echo PHP_EOL;



////////////////////////////////////////////////////////////////////////////////

// You can do the same when setting properties.

$response1->data->User->id = 5678;
$response2['data']['User']['id'] = 5678;


if (isset($response1->data['User']->id)) {
	echo "Response 1 User Id: ".$response1->data['User']->id.PHP_EOL;
}
if (isset($response2['data']->User['id'])) {
	echo "Response 1 User Id: ".$response2['data']->User['id'].PHP_EOL;
}

echo PHP_EOL;
