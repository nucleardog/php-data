<?php

declare(strict_types=1);
require(__DIR__.'/../vendor/autoload.php');

use Nucleardog\Data\Data;

////////////////////////////////////////////////////////////////////////////////

// The data class can be instantiated with an object, array, ArrayAccess (with limitations), etc.

$data1 = new Data([
	'a' => 1,
	'b' => 2,
	'c' => 'cats',
	'd' => 'dogs',
]);

$data2 = new Data((object)[
	'a' => 1,
	'b' => 2,
	'c' => 'cats',
	'd' => 'dogs',
]);

echo "Data 1 type: ".gettype($data1->unwrap()).PHP_EOL;
echo "Data 2 type: ".gettype($data2->unwrap()).PHP_EOL;
echo PHP_EOL;



////////////////////////////////////////////////////////////////////////////////

// You can then mix accessing keys via object properties or array syntax freely.

// Object properties
if (isset($data1->a)) {
	echo "Data 1, Object Property, `a`: ".$data1->a.PHP_EOL;
}
if (isset($data2->a)) {
	echo "Data 2, Object Property, `a`: ".$data2->a.PHP_EOL;
}

// Array offsets
if (isset($data1['a'])) {
	echo "Data 1, Array Offset, `a`: ".$data1['a'].PHP_EOL;
}
if (isset($data2['a'])) {
	echo "Data 2, Array Offset, `a`: ".$data2['a'].PHP_EOL;
}

echo PHP_EOL;



////////////////////////////////////////////////////////////////////////////////

// You can set and unset the properties the same way.
echo sprintf("It's raining %s and %s.", $data1->c, $data2['d']).PHP_EOL;
$data1['c'] = 'dogs';
$data2->d = 'cats';
echo sprintf("It's raining %s and %s.", $data1['c'], $data2->d).PHP_EOL;

echo PHP_EOL;

unset($data1->c);
unset($data2['d']);
echo sprintf('Does data1 contain c? %s', isset($data1['c']) ? 'yes' : 'no').PHP_EOL;
echo sprintf('Does data2 contain d? %s', isset($data2->d) ? 'yes' : 'no').PHP_EOL;

echo PHP_EOL;
