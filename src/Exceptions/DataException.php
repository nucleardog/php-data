<?php

declare(strict_types=1);

namespace Nucleardog\Data\Exceptions;

class DataException extends \Exception
{
}
