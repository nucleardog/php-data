<?php

declare(strict_types=1);

namespace Nucleardog\Data\Exceptions;

class UnsupportedDataTypeException extends DataException
{
	public function __construct(mixed $data)
	{
		parent::__construct(sprintf('Unsupported data type: %s', $this->getDataTypeDescription($data)));
	}

	private function getDataTypeDescription(mixed $data): string
	{
		if (is_object($data)) {
			return get_class($data);
		} else {
			return gettype($data);
		}
	}
}