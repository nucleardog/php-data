<?php

declare(strict_types=1);

namespace Nucleardog\Data\Iterators;

class ObjectPropertyReferenceIterator implements \Iterator
{
	private array $keys;
	private int $idx;

	public function __construct(
		private object $object,
	) {
	}

	public function &current(): mixed
	{
		$rc = new \ReflectionClass($this->object);

		try
		{
			$rp = $rc->getProperty($this->keys[$this->idx]);
			if ($rp->isReadOnly()) {
				$value = $this->object->{$this->keys[$this->idx]};
				return $value;
			} else {
				return $this->object->{$this->keys[$this->idx]};
			}
		}
		catch (\ReflectionException)
		{
			return $this->object->{$this->keys[$this->idx]};
		}
	}

	public function key(): mixed
	{
		return $this->keys[$this->idx];
	}

	public function next(): void
	{
		$this->idx++;
	}

	public function rewind(): void
	{
		$this->keys = array_keys(get_object_vars($this->object));
		$this->idx = 0;
	}

	public function valid(): bool
	{
		return $this->idx >= 0 && $this->idx < sizeof($this->keys);

	}

}