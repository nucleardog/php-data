<?php

declare(strict_types=1);

namespace Nucleardog\Data\Iterators;
use Nucleardog\Data\Data;
use Nucleardog\Data\Accessor\Accessor;
use Nucleardog\Data\Exceptions\UnsupportedDataTypeException;

class DataWrapIterator implements \Iterator
{
	public function __construct(
		private \Iterator $iterator,
		private string $class,
	) {
	}

	public function &current(): mixed
	{
		$value = $this->iterator->current();
		try
		{
			$return = $value;
			if (!($return instanceof Accessor) &&
			    !($return instanceof Data)) {
				$return = Accessor::for($value);
			}
			if ($return instanceof Accessor) {
				$class = $this->class;
				$return = new $class($return);
			}
			return $return;
		}
		catch (UnsupportedDataTypeException)
		{
		}
		return $value;
	}

	public function key(): mixed
	{
		return $this->iterator->key();
	}

	public function next(): void
	{
		$this->iterator->next();
	}

	public function rewind(): void
	{
		$this->iterator->rewind();
	}

	public function valid(): bool
	{
		return $this->iterator->valid();
	}

}