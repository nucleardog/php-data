<?php

declare(strict_types=1);

namespace Nucleardog\Data\Iterators;

class ArrayReferenceIterator implements \Iterator
{
	private array $keys;
	private int $idx;

	public function __construct(
		private array &$data,
	) {
	}

	public function &current(): mixed
	{
		return $this->data[$this->keys[$this->idx]];
	}

	public function key(): mixed
	{
		return $this->keys[$this->idx];
	}

	public function next(): void
	{
		$this->idx++;
	}

	public function rewind(): void
	{
		$this->keys = array_keys($this->data);
		$this->idx = 0;
	}

	public function valid(): bool
	{
		return $this->idx >= 0 && $this->idx < sizeof($this->keys);

	}

}