<?php

namespace Nucleardog\Data\Accessor;
use Nucleardog\Data\Exceptions\UnsupportedDataTypeException;
use ArrayAccess;

class ArrayAccessAccessor extends Accessor
{
	use Concerns\SamePropertiesAndKeys;

	public function __construct(
		private ArrayAccess $data,
	) {
	}

	public function __clone(): void
	{
		$this->data = clone $this->data;
	}

	public function count(): int
	{
		// FIXME: If the ArrayAccess object doesn't implement Countable, this will
		//        fail or return incorrect results.
		return count($this->data);
	}

	public function getIterator(): \Traversable
	{
		if ($this->data instanceof \IteratorAggregate) {
			return $this->data->getIterator();
		} else {
			// FIXME: This also likely doesn't work with ArrayAccess.
			//        Should check for IteratorAggregate class.
			return new \ArrayIterator($this->data);
		}
	}

	public function has(mixed $key): bool
	{
		return isset($this->data[$key]);
	}

	public function &get(mixed $key): mixed
	{
		$data = $this->data[$key];
		try
		{
			return Accessor::for($data);
		}
		catch (UnsupportedDataTypeException)
		{
		}

		return $data;
	}

	public function set(mixed $key, mixed $value): void
	{
		$this->data[$key] = $value;
	}

	public function forget(mixed $key): void
	{
		unset($this->data[$key]);
	}

	public function unwrap(): mixed
	{
		return $this->data;
	}

}