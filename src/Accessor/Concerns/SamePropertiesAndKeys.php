<?php

declare(strict_types=1);

namespace Nucleardog\Data\Accessor\Concerns;

trait SamePropertiesAndKeys
{

	protected abstract function has(mixed $key): bool;
	protected abstract function &get(mixed $key): mixed;
	protected abstract function set(mixed $key, mixed $value): void;
	protected abstract function forget(mixed $key): void;

	public function hasProperty(mixed $key): bool { return $this->has($key); }
	public function &getProperty(mixed $key): mixed { return $this->get($key); }
	public function setProperty(mixed $key, mixed $value): void { $this->set($key, $value); }
	public function forgetProperty(mixed $key): void { $this->forget($key); }

	public function hasOffset(mixed $key): bool { return $this->has($key); }
	public function &getOffset(mixed $key): mixed { return $this->get($key); }
	public function setOffset(mixed $key, mixed $value): void { $this->set($key, $value); }
	public function forgetOffset(mixed $key): void { $this->forget($key); }

}