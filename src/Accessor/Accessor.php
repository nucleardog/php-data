<?php

namespace Nucleardog\Data\Accessor;
use Nucleardog\Data\Exceptions\UnsupportedDataTypeException;
use Nucleardog\Data\Contracts\SeparateOffsetsAndProperties;

abstract class Accessor implements \IteratorAggregate, \Countable
{

	public static function for(mixed &$data): Accessor
	{
		return match (true) {
			is_object($data) && ($data instanceof \ArrayAccess) && !($data instanceof SeparateOffsetsAndProperties) => new MergeOffsetsAndPropertiesAccessor($data, new ArrayAccessAccessor($data), new ObjectAccessor($data)),
			is_object($data) && ($data instanceof \ArrayAccess) && ($data instanceof SeparateOffsetsAndProperties) => new SeparateOffsetsAndPropertiesAccessor($data, new ArrayAccessAccessor($data), new ObjectAccessor($data)),
			is_object($data) && !($data instanceof \ArrayAccess) => new ObjectAccessor($data),
			is_array($data) => new ArrayAccessor($data),
			default => throw new UnsupportedDataTypeException($data),
		};
	}

	public function call(string $method, array $args): mixed
	{
		return $this->unwrap()->$method(...$args);
	}

	public abstract function count(): int;
	public abstract function getIterator(): \Traversable;

	public abstract function hasProperty(mixed $key): bool;
	public abstract function &getProperty(mixed $key): mixed;
	public abstract function setProperty(mixed $key, mixed $value): void;
	public abstract function forgetProperty(mixed $key): void;

	public abstract function hasOffset(mixed $key): bool;
	public abstract function &getOffset(mixed $key): mixed;
	public abstract function setOffset(mixed $key, mixed $value): void;
	public abstract function forgetOffset(mixed $key): void;

	public abstract function unwrap(): mixed;
	public abstract function __clone(): void;

}