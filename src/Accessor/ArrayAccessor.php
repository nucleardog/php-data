<?php

namespace Nucleardog\Data\Accessor;
use Nucleardog\Data\Iterators\ArrayReferenceIterator;

class ArrayAccessor extends Accessor
{
	use Concerns\SamePropertiesAndKeys;

	public function __construct(
		private array &$data,
	) {
	}

	public function __clone(): void
	{
		$data = $this->data;
		$this->data = $data;
	}

	public function count(): int
	{
		return count($this->data);
	}

	public function getIterator(): \Traversable
	{
		return new ArrayReferenceIterator($this->data);
	}

	protected function has(mixed $key): bool
	{
		return isset($this->data[$key]);
	}

	protected function &get(mixed $key): mixed
	{
		return $this->data[$key];
	}

	protected function set(mixed $key, mixed $value): void
	{
		$this->data[$key] = $value;
	}

	protected function forget(mixed $key): void
	{
		unset($this->data[$key]);
	}

	public function unwrap(): mixed
	{
		return $this->data;
	}

}