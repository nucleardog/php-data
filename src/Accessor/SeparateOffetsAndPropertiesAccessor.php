<?php

namespace Nucleardog\Data\Accessor;

class SeparateOffsetsAndPropertiesAccessor extends Accessor
{

	public function __construct(
		private object $data,
		private Accessor $offsetAccessor,
		private Accessor $propertyAccessor,
	) {
	}

	public function __clone(): void
	{
		$this->data = clone $this->data;
	}

	public function count(): int
	{
		// TODO: Handle when countable not implemented
		return count($this->data);
	}

	public function getIterator(): \Traversable
	{
		// TODO: Handle when iterator isn't defined.
		return $this->data->getIterator();
	}

	public function hasOffset(mixed $key): bool
	{
		return $this->offsetAccessor->hasOffset($key);
	}

	public function &getOffset(mixed $key): mixed
	{
		return $this->offsetAccessor->getOffset($key);
	}

	public function setOffset(mixed $key, mixed $value): void
	{
		$this->offsetAccessor->setOffset($key, $value);
	}

	public function forgetOffset(mixed $key): void
	{
		$this->offsetAccessor->forgetOffset($key);
	}

	public function hasProperty(mixed $key): bool
	{
		return $this->propertyAccessor->hasProperty($key);
	}

	public function &getProperty(mixed $key): mixed
	{
		return $this->propertyAccessor->getProperty($key);
	}

	public function setProperty(mixed $key, mixed $value): void
	{
		$this->propertyAccessor->setProperty($key, $value);
	}

	public function forgetProperty(mixed $key): void
	{
		$this->propertyAccessor->forgetProperty($key);
	}

	public function unwrap(): mixed
	{
		return $this->data;
	}

}