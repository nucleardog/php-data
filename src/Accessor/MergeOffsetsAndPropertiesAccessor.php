<?php

namespace Nucleardog\Data\Accessor;

class MergeOffsetsAndPropertiesAccessor extends Accessor
{

	public function __construct(
		private object $data,
		private Accessor $offsetAccessor,
		private Accessor $propertyAccessor,
	) {
	}

	public function __clone(): void
	{
		$this->data = clone $this->data;
	}

	public function count(): int
	{
		// TODO: Handle when countable not implemented
		return count($this->data);
	}

	public function getIterator(): \Traversable
	{
		// TODO: Handle when iterator isn't defined.
		return $this->data->getIterator();
	}

	public function hasOffset(mixed $key): bool
	{
		return $this->getOffsetAccessor($key)->hasOffset($key);
	}

	public function &getOffset(mixed $key): mixed
	{
		return $this->getOffsetAccessor($key)->getOffset($key);
	}

	public function setOffset(mixed $key, mixed $value): void
	{
		$this->getOffsetAccessor($key)->setOffset($key, $value);
	}

	public function forgetOffset(mixed $key): void
	{
		$this->getOffsetAccessor($key)->forgetOffset($key);
	}

	public function hasProperty(mixed $key): bool
	{
		return $this->getPropertyAccessor($key)->hasProperty($key);
	}

	public function &getProperty(mixed $key): mixed
	{
		return $this->getPropertyAccessor($key)->getProperty($key);
	}

	public function setProperty(mixed $key, mixed $value): void
	{
		$this->getPropertyAccessor($key)->setProperty($key, $value);
	}

	public function forgetProperty(mixed $key): void
	{
		$this->getPropertyAccessor($key)->forgetProperty($key);
	}

	public function unwrap(): mixed
	{
		return $this->data;
	}

	private function getOffsetAccessor(string $name): Accessor
	{
		if (!$this->isOffset($name) && $this->isProperty($name)) {
			return $this->propertyAccessor;
		} else {
			return $this->offsetAccessor;
		}
	}

	private function getPropertyAccessor(string $name): Accessor
	{
		if (!$this->isProperty($name) && $this->isOffset($name)) {
			return $this->offsetAccessor;
		} else {
			return $this->propertyAccessor;
		}
	}

	private function isProperty(string $name): bool
	{
		if (in_array($name, array_keys(get_object_vars($this->data)))) {
			return true;
		}

		if (method_exists($this->data, '__isset') &&
			$this->data->__isset($name) === true)
		{
			return true;
		}

		return false;

	}

	private function isOffset(string $name): bool
	{
		if (method_exists($this->data, 'offsetExists') &&
			$this->data->offsetExists($name)) {
			return true;
		}

		return false;
	}

}