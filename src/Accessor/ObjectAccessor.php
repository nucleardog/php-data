<?php

namespace Nucleardog\Data\Accessor;
use Nucleardog\Data\Iterators\ObjectPropertyReferenceIterator;
use ArrayObject;

class ObjectAccessor extends Accessor
{
	use Concerns\SamePropertiesAndKeys;

	public function __construct(
		private object $data,
	) {
	}

	public function __clone(): void
	{
		$this->data = clone $this->data;
	}

	public function count(): int
	{
		return count(new ArrayObject($this->data));
	}

	public function getIterator(): \Traversable
	{
		return new ObjectPropertyReferenceIterator($this->data);
	}

	protected function has(mixed $key): bool
	{
		return isset($this->data->$key);
	}

	protected function &get(mixed $key): mixed
	{
		$rc = new \ReflectionClass($this->data);
		$rp = $rc->getProperty($key);

		if ($rp->isReadOnly()) {
			$value = $this->data->$key;
			return $value;
		} else {
			return $this->data->$key;
		}
	}

	protected function set(mixed $key, mixed $value): void
	{
		$this->data->$key = $value;
	}

	protected function forget(mixed $key): void
	{
		unset($this->data->$key);
	}

	public function unwrap(): mixed
	{
		return $this->data;
	}

}