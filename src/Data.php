<?php

declare(strict_types=1);

namespace Nucleardog\Data;
use Nucleardog\Data\Accessor\Accessor;

class Data implements \ArrayAccess, \IteratorAggregate
{
	private readonly Accessor $accessor;

	public function __construct(mixed $data)
	{
		if ($data instanceof Data)
			$this->accessor = $data->accessor;
		else if ($data instanceof Accessor)
			$this->accessor = $data;
		else
			$this->accessor = Accessor::for($data);
	}

	public function clone(): Data
	{
		return new static(clone $this->accessor);
	}

	public function getIterator(): \Traversable
	{
		return new Iterators\DataWrapIterator($this->accessor->getIterator(), static::class);
	}

	public function offsetExists(mixed $offset): bool
	{
		return $this->accessor->hasOffset($offset);
	}

	public function offsetGet(mixed $offset): mixed
	{
		if (!$this->offsetExists($offset))
			throw new \OutOfBoundsException(sprintf('Data does not contain key %s', (string)$offset));
		return $this->wrap($this->accessor->getOffset($offset));
	}

	public function offsetSet(mixed $offset, mixed $value): void
	{
		$this->accessor->setOffset($offset, $value);
	}

	public function offsetUnset(mixed $offset): void
	{
		$this->accessor->forgetOffset($offset);
	}

	public function __get(string $property): mixed
	{
		if (!$this->__isset($property))
			throw new \OutOfBoundsException(sprintf('Data does not contain key %s', $property));
		return $this->wrap($this->accessor->getProperty($property));
	}

	public function __set(string $property, mixed $value): void
	{
		$this->accessor->setProperty($property, $value);
	}

	public function __isset(string $property): bool
	{
		return $this->accessor->hasProperty($property);
	}

	public function __unset(string $property): void
	{
		$this->accessor->forgetProperty($property);
	}

	public function unwrap(): mixed
	{
		return $this->accessor->unwrap();
	}

	public function has(array|string $key): bool
	{
		try
		{
			[$accessor, $key] = $this->getAccessorForPath($key, 'Property');
		}
		catch (\OutOfBoundsException)
		{
			return false;
		}
		return $accessor->hasProperty($key);
	}

	public function get(array|string $key): mixed
	{
		[$accessor, $key] = $this->getAccessorForPath($key, 'Property');
		if (!$accessor->hasProperty($key)) {
			throw new \OutOfBoundsException(sprintf('Data does not contain key %s', $key));
		}
		return $this->wrap($accessor->getProperty($key));
	}

	public function set(array|string $key, mixed $value): void
	{
		[$accessor, $key] = $this->getAccessorForPath($key, 'Property');
		$accessor->setProperty($key, $value);
	}

	public function forget(array|string $key): void
	{
		try
		{
			[$accessor, $key] = $this->getAccessorForPath($key, 'Property');
		}
		catch (\OutOfBoundsException)
		{
			return;
		}
		$accessor->forgetProperty($key);
	}

	private function getAccessorForPath(array|string $path, string $type): array
	{
		if (is_string($path)) {
			$path = explode('.', $path);
		}
		$key = array_pop($path);

		$accessor = $this->accessor;

		foreach ($path as $next) {
			if (!$accessor->{'has'.$type}($next)) {
				throw new \OutOfBoundsException(sprintf('Data does not contain key %s', $next));
			}
			$accessor = $this->wrapInAccessor($accessor->{'get'.$type}($next));
			if (!($accessor instanceof Accessor)) {
				throw new \OutOfBoundsException(sprintf('Path "%s" traverses unsupported data type.', implode('.', array_merge($path, [$key]))));
			}
		}

		return [$accessor, $key];
	}

	private function wrap(mixed &$value): mixed
	{
		return $this->wrapInData($this->wrapInAccessor($value));
	}

	private function wrapInAccessor(mixed &$value): mixed
	{
		if (!($value instanceof Accessor) &&
		    !($value instanceof Data)) {
			try
			{
				return Accessor::for($value);
			}
			catch (Exceptions\UnsupportedDataTypeException)
			{
			}
		}
		return $value;
	}

	private function wrapInData(mixed $value): mixed
	{
		if ($value instanceof Accessor) {
			$data = new static($value);
			return $data;
		} else {
			return $value;
		}
	}

	public function __call(string $method, array $args): mixed
	{
		// TODO: Macroable

		// If Laravel's Collections are available, then expose the data object
		// as a Collection.
		if (class_exists(\Illuminate\Support\Collection::class) &&
		    method_exists(\Illuminate\Support\Collection::class, $method)) {
			// Convert our data to a collection
			$collection = $this->toCollection();
			// Call the collection method
			$result = call_user_func_array([$collection, $method], $args);

			// If the collection returned a new collection instance, then get
			// the data back out of it and wrap it in a new Data instance.
			if ($result instanceof \Illuminate\Support\Collection) {
				return new static($result->all());
			// Otherwise use our normal wrapping to wrap arrays and things with
			// a new Data instance.
			} else {
				return $this->wrap($result);
			}
		}

		return $this->accessor->call($method, $args);
		//throw new \BadMethodCallException(sprintf('Method Data::%s does not exist', $method));
	}

	public function toCollection(): \Illuminate\Support\Collection
	{
		return new \Illuminate\Support\Collection($this->toArray());
	}

	public function toArray(bool $recursive = false): array
	{
		$values = iterator_to_array($this);
		if ($recursive) {
			$values = array_map(fn($value) => $value instanceof Data ? $value->toArray($recursive) : $value, $values);
		}
		return $values;
	}

}