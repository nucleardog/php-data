# data

Bringing a unified interface to arrays and objects.

# Examples

See [the examples](examples/) for additional information.

# Known Limitations

* Modifying values nested under an object implementing ArrayAccess will
  not be reflected on the original object.

# Legal

Copyright 2024 Adam Pippin <hello@adampippin.ca>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this project except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
